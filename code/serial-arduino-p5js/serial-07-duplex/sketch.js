let inData; // for incoming serial data

// variables for the circle to be drawn:
let locH = 0;
let locV = 0;
let circleColor = 255;

const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  background(0);
  setPorts(); //Configurar puerto serial (set-webserial.js)
}


function draw() {
  background(0); // black background
  fill(circleColor); // fill depends on the button
  ellipse(locH, locV, 50, 50); // draw the circle
}

// read any incoming data as a string
// (assumes a newline at the end of it):
function serialEvent() {
  // read a string from the serial port
  // until you get carriage return and newline:
  var inString = serial.readStringUntil("\r\n");
  //let inString = serial.read();

  //check to see that there's actually a string there:
  if (inString) {
    if (inString !== "hello") {
      //console.log(inString);

      // if you get hello, ignore it
      // split the string on the commas:
      var sensors = split(inString, ",");
      if (sensors.length > 2) {
        // if there are three elements
        // element 0 is the locH:
        locH = map(sensors[0], 0, 1023, 0, width);
        // element 1 is the locV:
        locV = map(sensors[1], 0, 1023, 0, height);
        // element 2 is the button:
        circleColor = 255 - sensors[2] * 255;
        // send a byte back to prompt for more data:
        serial.write("x");
      }
    }
  }
}