let slider1, slider2, slider3;
let sliderW = 300;
let posX, posY;

const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  background(0);
  posX = width / 2;
  posY = height / 2;

  //Slider1 para led rojo
  slider1 = createSlider(0, 255, 100);
  slider1.position(posX - sliderW / 2, posY - 25);
  slider1.style('width', sliderW + 'px');

  //Slider2 para led verde
  slider2 = createSlider(0, 255, 100);
  slider2.position(posX - sliderW / 2, posY);
  slider2.style('width', sliderW + 'px');

  //Slider3 para led azul
  slider3 = createSlider(0, 255, 100);
  slider3.position(posX - sliderW / 2, posY + 25);
  slider3.style('width', sliderW + 'px');

  setPorts(); //Configurar puerto serial (set-webserial.js)
}

function draw() {
  let val1 = slider1.value();
  let val2 = slider2.value();
  let val3 = slider3.value();
  background(val1, val2, val3); //Cambiamos color de fondo en función del valor de los sliders

  //envío serial de un único string. P.ej: "123,233,023"
  serial.print(nf(val1, 3)); //utilizamos nf() para que siempre mantenga 3 cifras. Por ejemplo: 004
  serial.print(',');
  serial.print(nf(val2, 3));
  serial.print(',');
  serial.println(nf(val3, 3));
  
  //Mostramos los valores en el canvas
  textAlign(CENTER);
  textSize(32);
  text("R: "+val1+", G: "+val2+", B: "+val3, posX, 100);
}

function serialEvent() {
  //Mostramos el mensaje de vuelta que nos envía arduino por serial
  let inString = serial.readLine();
  if (inString !== null) {
    console.log(inString);
  }
}