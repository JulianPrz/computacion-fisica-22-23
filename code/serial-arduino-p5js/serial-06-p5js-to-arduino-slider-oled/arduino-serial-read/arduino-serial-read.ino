// led
const int ledPin = 11;

//oled
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

//SCK A5
//SDA A4

#define OLED_RESET -1

int potPin = A0;
int valMap = 0;

Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

//Serial
int incomingByte;  // a variable to read incoming serial data into

void setup() {
  //oled
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  //serial
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  while (Serial.available() <= 0) {
    Serial.println("hello");  // send a starting message
    delay(300);               // wait 1/3 second
  }
}

void loop() {
  oled.clearDisplay();  // Clear display must be used to clear text etc
  
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    //led
      analogWrite(ledPin, incomingByte);

  }
    //oled
    //oled.setCursor(0, 0);
    // oled.print("slider");
    // oled.setCursor(30, 50);  // 30=H; 50=V
    valMap = map(incomingByte, 0, 255, 0, 128);
    oled.fillRect(0, 3, valMap, 3, 1);  // 2=V from left; 61=H from top; val/8= lenght of bar ie, val/16 is half screen with full value; 3= thickness of slide bar; 1= ? colour or brightness
    //oled.println(incomingByte);
    oled.display();
    
    //delay(1);
}