  const int buttonPin = 2;      // digital input

  void setup() {
          Serial.begin(9600);
          pinMode(buttonPin, INPUT_PULLUP);
  }

  void loop() {
          int sensorValue = analogRead(A0);
          Serial.print(sensorValue);
          Serial.print(",");

          sensorValue = analogRead(A1);
          Serial.print(sensorValue);
          Serial.print(",");

          sensorValue = digitalRead(buttonPin);
          Serial.println(sensorValue);
  }