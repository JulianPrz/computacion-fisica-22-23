const int ledPin = 11;
int incomingByte;  // a variable to read incoming serial data into

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  while (Serial.available() <= 0) {
    Serial.println("hello");  // send a starting message
    delay(300);               // wait 1/3 second
  }
}

void loop() {
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    analogWrite(ledPin, incomingByte);
  }
}