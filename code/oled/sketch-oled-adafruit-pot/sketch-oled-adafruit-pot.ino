#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

//SCK A5
//SDA A4

#define OLED_RESET -1

int potPin = A0;
int val = 0;
int valMap = 0;

Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup() {
  Serial.begin(9600);
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
}

void loop() {
  oled.setCursor(0, 5);
  oled.print("Potentiometer");
  oled.setCursor(0, 20);
  oled.print("Bar Graph");
  oled.setCursor(30, 50);  // 30=H; 50=V
  val = analogRead(potPin);
  valMap = map(val, 0, 1023, 0, 128);
  oled.fillRect(2, 61, valMap, 3, 1);  // 2=V from left; 61=H from top; val/8= lenght of bar ie, val/16 is half screen with full value; 3= thickness of slide bar; 1= ? colour or brightness
  oled.println(val);
  oled.display();
  oled.clearDisplay();  // Clear display must be used to clear text etc
}