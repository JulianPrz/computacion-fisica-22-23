#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE: E2 Comunicación serial p5js - Arduino
#+SUBTITLE: Computación Física
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_EXTRA_CSS: ../../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em">%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><br><p>Máster de Diseño Interactivo</p><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
- Habiendo visto la conexión entre p5js y arduino tenéis que preparar un ejemplo interactivo
- Puedes conectar un botón y un pot o ambos y que la interacción se refleje en p5js
- Un ejemplo simple podría ser el juego de [[https://en.wikipedia.org/wiki/Etch_A_Sketch][etch&sketch]] (2pots mín.)
- Repasa los contenidos para la conexión serial
#+reveal: split
#+attr_html: :height 400px :display block
#+CAPTION: Esquema 2 pots + 1 switch
[[../../../img/04/esquema-e2.png]]  
* Requisitos
  - Comentarios al inicio del sketch de lo que hace tu código
  - Usar al menos un botón y un potenciómetro
  - Bien establecida la comunicación serial entre arduino y p5js
  - Vídeo demostración del ejercicio y código del proyecto en un zip
* Evaluación
  - Se valorará la calidad del código, los comentarios, ideación,
    estética, cumplir con los requisitos y la entrega a tiempo
* Entrega
- La entrega finaliza el día *15 de marzo* y se pueden resolver dudas por Slack
- Se entrega a través del campus virtual con la siguiente nomenclatura: ="e2-apellido-nombre.zip"=  
