#+STARTUP: indent
#+STARTUP: overview

#+OPTIONS: toc:nil
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport

* Computación Física  
** Presentación asignatura
Haremos un repaso a cómo se estructurará la asignatura en el semestre, veremos criterios de evaluación y aspectos a tener en cuenta     
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/00-intro/00-presentacion.html][00 - Presentación]]
** I - Principios de computación física
Primero nos introduciremos en la computación física a través de un repaso histórico de la evolución de la computación, para después comenzar a ver los diferentes hitos que han ido conformando lo que entendemos hoy como computación física y programación creativa
*** temas
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/00-intro/01-intro-asignatura.html][01 - Introducción a la Computación Física]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/01-computacion-fisica/02-intro-arduino.html][02 - Introducción a Arduino]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/01-computacion-fisica/04-arduino-II.html][04 - Arduino II]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/01-computacion-fisica/05-arduino-III.html][05 - Arduino III]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/01-computacion-fisica/06-arduino-IV.html][06 - Arduino IV]]
** II - Elementos de Programación Creativa
   En este bloque aprenderemos a programar con [[https://p5js.org][p5js]], una librería de Javascript de código abierto, de fácil utilización, y que sirve como medio para la enseñanza y producción de proyectos multimedia e interactivos de diseño digital.
   - Página de [[https://p5js.org/es/download/][descarga]] de la librería
   - Manual de [[https://p5js.org/es/reference/][referencia]]
*** Temas
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/01-computacion-fisica/03-intro-p5js][03 - Introducción a p5js]] 
** III - Casos de Estudio
*** 1. Análisis y discusión de instalaciones interactivas
    Un ciclo de presentaciones / debates sobre casos de estudio. En el
    que los alumnos estarán invitados a seleccionar, y presentar en
    clase, algunos casos de estudio analizando tanto los aspectos
    técnicos como, sobre todo, las modalidades de interacción y las
    bases conceptuales. Los casos presentados serán objeto de un
    debate colectivo y servirán para poder comentar, de manera
    extemporánea, otros casos análogos y algunos casos “notable” de
    autores consolidados.
** Prácticas
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/04_Practicas/e1-map-analogico/e1.html][e1 - lectura analógica]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/04_Practicas/e2-com-serial/E2.html][e2 - comunicación serial p5js - arduino]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/02-practicas/e3-prototipo/e3.html][e3 - prototipo]]
- [[https://julianprz.gitlab.io/computacion-fisica-22-23/main/docs/02-practicas/e4-casos-estudio/e4-casos-estudio.html][e4 - caso de estudio]]
** Se queda fuera                                                  :noexport:
*** Temas bloque II
    1. Análisis de señales sonoras
    2. API’s y acceso a datos 
    3. Técnicas de Computer Vision 
    4. Cámaras de profundidad 
    5. Conexión a sensores 
    6. Actuadores
    7. Dispositivos en red 
    8. Conexión Arduino y Processing 
       
       
